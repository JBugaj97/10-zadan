#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct dane_vektora
{
	double maxium;
	double minium;
	double srednia;
	double mediana;
};
string zadania(string y);

void onest(string nazwa, vector <int> tab);
void secondstr(string nazwa, vector <string> tab);
void threedouble(string nazwa, vector <double> tab);
vector <int> lector(int w, int e, vector <int> r, int t);
int maxium_int();
vector <int> odwracanie_wektora_origin(vector<int>&w);
vector <int> odwracanie_wektora_copy(vector<int>w);
vector <string> odwracanie_wektora_origin_str(vector<string>&w);
vector <string> odwracanie_wektora_copy_str(vector<string>w);
void sort_imion(int ilosc);
vector <double> tart(vector <double> &koszt, vector <double> &waga);
int maxium(vector <int> vektor);
dane_vektora wektor(vector <double> vektor);