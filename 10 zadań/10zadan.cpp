#include <iostream>
#include <vector>
#include "Nag�owek.h"

using namespace std;

int main()
{
	while (true)
	{
		cout << "zadanie 6" << endl;
		cout << endl;
		cout << "Zadanie 1" << endl;
		cout << "Zadanie 2" << endl;
		cout << "Zadanie 3" << endl;
		cout << "Zadanie 4" << endl;
		cout << "Zadanie 5" << endl;
		cout << "Zadanie 6" << endl;
		cout << "Zadanie 7" << endl;
		cout << "Zadanie 8" << endl;
		cout << "Zadanie 9" << endl;
		cout << "Zadanie 10" << endl;
		cout << endl;
		cout << "Wybierz zadanie: ";

		int wybierz;
		while (!(cin >> wybierz))
		{
			cout << endl;
			cout << zadania("Czy wybranie zadania jest za trudne?") << endl;
			cout << zadania("Wpisz nr zadania poprawnie: ");
			cin.clear();
			cin.ignore(999999, '\n');
		}
		system("cls");
		cout << "Wybrano zadanie " << wybierz << endl << endl;
		switch (wybierz)
		{
		case 1:
		{
			cout << zadania("1. Napisz funkcj� onest(), kt�ra wypisuje na ekran vector zmiennych int na cout. ") << endl;
			cout << zadania("podaj jej dwa argumenty : string jako nazw� wyj�cia oraz vector.") << endl;
			cout << zadania("////////////////////////////////////////////////") << endl;
			cout << endl;
			vector <int> tab1= { 268452, 9525345, 252, 32434, 65212, 32321 };
			onest("Liczby:", tab1);
		}
		break;
		case 2:
		{
			cout << zadania("2. Stw�rz vetkor ci�gu Fibonacciego i wypisz je przy pomocy funkcji z zadania 1. ") << endl;
			cout << zadania("Do stworzenia ci�gu Fibonacciego u�yj funkcji lector (w, e, r, t), gdzie w i e s� typu int, ") << endl;
			cout << zadania("w jest pustym vectorem <int> a t jest elementem, kt�ry nale�y wype�ni� w vektorze w. w[0] b�dzie w, w[1] b�dzie e.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			int nr1, nr2, elementy;
			vector <int> tab2;
			cout << zadania("Podaj pierwsz� liczb� ci�gu: x = ");
			cin >> nr1;
			cout << zadania("Podaj drug� liczb� ci�gu: y = ");
			cin >> nr2;
			cout << zadania("Podaj ilo�� element�w ci�gu: n = ");
			cin >> elementy;
			cout << endl;
			onest("Ci�g Fibonacciego", lector(nr1, nr2, tab2, elementy));
		}
		break;
		case 3:
		{
			cout << zadania("3. Int mo�e przechowywa� liczby ca�kowite tylko do pewnej ograniczonej warto�ci. ") << endl;
			cout << zadania("Znajd� przybli�enie tego ograniczenia u�ywaj�c funkcji lector.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			cout << zadania("za pomoc� funkcji tart najwi�ksza warto�� zmiennej typu int wynosi: ") << maxium_int() << endl;
		}
		break;
		case 4:
		{
			cout << zadania("4. Stw�rz dwie funkcje, kt�re odwracaj� kolejno�� element�w w vector<int>, np. 1,3,5,7,9 ") << endl;
			cout << zadania("zostanie zamienione na 9,7,5,3,1. Pierwsza funkcja powinna tworzy� nowy vektor ") << endl;
			cout << zadania("pozostawiaj�c pierwotny bez zmian. Druga funkcja powinna zmieni� oryginalny vektor nie ") << endl;
			cout << zadania("tworz�c dodatkowych innych wektor�w.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			vector <int> tab4;
			int ilosc;
			cout << zadania("Podaj ile chcesz wpisa� element�w vektora <int>: ");
			cin >> ilosc;
			cout << "Podaj " << ilosc << zadania(" element�w:") << endl;
			int zaladuj;
			for (int a = 0; a < ilosc; ++a)
			{
				cin >> zaladuj;
				tab4.push_back(zaladuj);
			}
			system("cls");
			onest("vektor origin:", tab4);
			cout << "-------------------------------------" << endl;
			onest("vektor zwr�cony przez funckje \"odwracanie_wektora_copy\":", odwracanie_wektora_copy(tab4));
			onest("vektor origin:", tab4);
			cout << "-------------------------------------" << endl;
			onest("vektor zwr�cony przez funckje \"odwracanie_wektora_copy\":", odwracanie_wektora_origin(tab4));
			onest("vektor orgin:", tab4);
			cout << "-------------------------------------" << endl;
		}
		break;
		case 5:
		{
			cout << zadania("5. Napisz wersj� funkcji z �wiczenia 4, ale dla vetor<string>") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			vector <string> tab5;
			int ilosc;
			cout << zadania("Podaj ile chcesz wpisa� element�w vektora <string>: ");
			cin >> ilosc;
			cout << "Podaj " << ilosc << zadania(" element�w:") << endl;
			string zaladuj;
			for (int a = 0; a < ilosc; ++a)
			{
				cin >> zaladuj;
				tab5.push_back(zaladuj);
			}
			system("cls");
			secondstr("vektor origin:", tab5);
			cout << "-------------------------------------" << endl;
			secondstr("vektor zwr�cony przez funckje \"odwracanie_wektora_copy\":", odwracanie_wektora_copy_str(tab5));
			secondstr("vektor origin:", tab5);
			cout << "-------------------------------------" << endl;
			secondstr("vektor zwr�cony przez funckje \"odwracanie_wektora_copy\":", odwracanie_wektora_origin_str(tab5));
			secondstr("vektor origin:", tab5);
			cout << "-------------------------------------" << endl;
		}
		break;
		case 6:
		{
			cout << zadania("6. Stw�rz 5 imion w wektorze vector<sting> name, nast�pnie u�ytkownik ma poda� ich wiek, ") << endl;
			cout << zadania("kt�ry zostanie zapisany w vector<douoble> age. Wypisz pi�� par (name[i], age[i]). Nst�pnie ") << endl;
			cout << zadania("posortuj imiona (sort(name.begin(),name.end())) i wypisz ponownie parami. Zwr�� uwag�, aby ") << endl;
			cout << zadania("wetor age by� te� w poprawnej kolejno�ci. Podpowied�: przed sortowaniem wektora name ") << endl;
			cout << zadania("stworz jego kopi�, aby po sortowaniu mo�na odtworzy� odpowiedni� relacj� miedzy name i age.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			sort_imion(5);
		}
		break;
		case 7:
		{
			cout << zadania("7. Powt�rz zadanie 6 dla wektora o dowolnej liczbie imion.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << zadania("") << endl;
			sort_imion(0);
		}
		break;
		case 8:
		{
			cout << zadania("8. Napisz funkcj�, kt�ra dla dw�ch wektor�w vector<double> cena i waga oblicza index kt�ry ") << endl;
			cout << zadania("jest sum� koszt [i] * waga [i]. Pami�taj, aby rozmiary wektor�w si� zgadza�y.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << endl;
			vector <double> koszt = { 25.23, 6.51, 5.32, 2.37 };
			vector <double> waga = { 7.73, 1.51, 3.92, .58 };
			threedouble("Cena:", koszt);
			threedouble("Waga:", waga);
			threedouble("Index:", tart(koszt, waga));
		}
		break;
		case 9:
		{
			cout << zadania("9. Napisz funkcj� maxium(), kt�ra zwraca najwiekszy element vektora.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << zadania("") << endl;
			vector <int> tab9;
			int ilosc, zaladuj;
			cout << zadania("Podaj ile liczb wpisa�: ");
			cin >> ilosc;
			for (int a = 0; a < ilosc; ++a)
			{
				cout << a + 1 << ". ";
				cin >> zaladuj;
				tab9.push_back(zaladuj);
			}
			cout << zadania("Najwi�ksz� liczb� z podanych jest: ") << maxium(tab9) << endl;
		}
		break;
		case 10:
		{
			cout << zadania("10. Napisz funckja, kt�ra znajduje maksymaln� i minimaln� warto�� vektora oraz liczy jego ") << endl;
			cout << zadania("�redni� i median�. Nie u�ywaj zmiennych globalnych. Wynik zwr�� przez struct zawieraj�cy ") << endl;
			cout << zadania("rozwi�zania lub zwr�� wyniki przez referencje.") << endl;
			cout << zadania("////////////////////////////////////////////////////////////////////") << endl;
			cout << zadania("") << endl;
			vector <double> tab10;
			int ilosc;
			double zaladuj;
			dane_vektora dane_vektora;
			cout << zadania("Podaj ile liczb wpisa�: ");
			cin >> ilosc;
			for (int a = 0; a < ilosc; ++a)
			{
				cout << a + 1 << ". ";
				cin >> zaladuj;
				tab10.push_back(zaladuj);
			}
			dane_vektora = wektor(tab10);
			cout << endl;
			cout << zadania("Warto�� najwi�ksza: ") << dane_vektora.maxium << endl;
			cout << zadania("Warto�� najmniejsza: ") << dane_vektora.minium << endl;
			cout << zadania("�rednia: ") << dane_vektora.srednia << endl;
			cout << zadania("Mediana: ") << dane_vektora.mediana << endl;
		}
		break;
		default:
		{
			cout << "Nie ma takiego zadania." << endl;
		}
		}
		cout << endl;
		system("pause");
		system("cls");
	}
	return 0;
}